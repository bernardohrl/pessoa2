#ifndef PROFESSOR_H
#define PROFESSOR_H

#include "pessoa.hpp"

class Professor: public Pessoa
{
	private: 
		string curso;
		string disciplina;
		int quantmaterias;
		string formacao ;
	public:
		void Aluno();
		void Aluno(string nome, string idade, string telefone, string curso, string disciplina, int quantmaterias, string formacao);
		string getCurso();
		void setCurso(string curso);
		string getDisciplina();
		void setDisciplina(string disciplina);
		int getQuantMaterias();
		void setQuantMaterias(int quantmaterias);
		string getFormacao();
		void setFormacao(string formacao);
};

#endif
