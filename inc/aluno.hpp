#ifndef ALUNO_H
#define ALUNO_H

#include "pessoa.hpp"

class Aluno: public Pessoa
{
	private: 
		int matricula;
		int quantidadeDeCreditos;
		int semestre;
		float ira;
	public:
		void Aluno();
		void Aluno(string nome, string idade, string telefone, int matricula,int quantidadeDeCreditos, int semestre, float ira);
		int getMatricula();
		void setMatricula(int matricula);
		int getCreditos();
		void setCreditos(int quantidadeDeCreditos);
		int getSemestre();
		void setSemestre(int Semestre);
		float getIra();
		void setIra(float ira);
};

#endif
